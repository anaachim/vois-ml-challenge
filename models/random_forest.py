import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
from sklearn.metrics import mean_squared_error

path = "./data/data.csv"
df = pd.read_csv(path)

scaler = MinMaxScaler()
df_num = df.drop(columns=['Cultivar'])
df_scaled = scaler.fit_transform(df_num)
df_scaled = pd.DataFrame(df_scaled, columns=df_num.columns)

x = df_scaled[["Season", "NS", "GY", "IFP"]]
y = df_scaled.iloc[:, -2] # The target is the second last column, "MHG"
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state= 1)

regressor = RandomForestRegressor(n_estimators=10)
regressor.fit(x_train,y_train)
predicted_MHG = regressor.predict(x_test)
mse = mean_squared_error(y_test, predicted_MHG)
print(f"Mean Squared Error: {mse}")

input_data = pd.DataFrame({
    "Season": [2],
    "NS": [7.5],
    "GY": [3865.48],
    "IFP": [22.6]
})

col_names = df_num.columns.tolist()
input_data_full = pd.DataFrame(columns=col_names)
for col in input_data_full.columns:
    if col in input_data.columns:
        input_data_full[col] = input_data[col]
    else:
        input_data_full[col] = 0
input_data_scaled = scaler.transform(input_data_full)
input_data_scaled = pd.DataFrame(input_data_scaled, columns=col_names)[["Season", "NS", "GY", "IFP"]]
forecast_MHG = regressor.predict(input_data_scaled)
print(f"For the input data, the predicted MHG is {forecast_MHG}")

# The predicted value appears to be scaled. In order to see the real predicted value, I need to invert the transformation
