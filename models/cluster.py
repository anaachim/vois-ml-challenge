from sklearn.cluster import KMeans
from scipy.cluster import hierarchy
import matplotlib.pyplot as plt
import pandas as pd


# Creating dendrogram to find out how many classes should the kmeans algorithm use
path = "./data/data.csv"
df = pd.read_csv(path)
df = df.select_dtypes(include = ["number"])
temp = hierarchy.linkage(df, "single")
plt.figure()
dn = hierarchy.dendrogram(temp)
plt.show()


# Create kmeans algorithm

df_norm = (df-df.mean())/df.std()
kmeans = KMeans(n_clusters=3, random_state=1)
clusters = kmeans.fit_predict(df_norm)
df["Cluster"] = clusters

# Plot the clusters
for cluster_id in range(3):
    cluster_data = df[df['Cluster'] == cluster_id]
    plt.scatter(cluster_data['GY'], cluster_data['MHG'], label=f'Cluster {cluster_id}')

plt.xlabel('GY')
plt.ylabel('MHG')
plt.title('Clusters of Similar Cultivars')
plt.legend()
plt.show()