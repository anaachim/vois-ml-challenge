import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler

# Importing data.csv from data folder
path = "./data/data.csv"
df = pd.read_csv(path)
print(df.head())

### 1. Data Cleaning 

# Validity
print(df.dtypes)
print(df.describe())

plt.hist(df["NGP"])
plt.title("NGP Histogram")
plt.show()

plt.hist(df["NGL"])
plt.title("NGL Histogram")
plt.show()

plt.hist(df["MHG"])
plt.title("MHG Histogram")
plt.show()

plt.hist("GY")
plt.title("GY Histogram")
plt.show()

# Accuracy
# Completeness - searching and handling missing values
print(df.isnull().any()) # I get False for all columns so there are no missing values that need to be handled

# Consistency - searching and handling duplicated values
print(df.duplicated().any()) # I also get False here so this step is completed

# Uniformity - Identify any unwanted outliers
# Conclusions of these plots can be found in the word document attached to the project
plt.boxplot(df["PH"])
plt.title("PH Boxplot")
plt.show() 

plt.boxplot(df["IFP"])
plt.title("IFP Boxplot")
plt.show()

plt.boxplot(df["NLP"])
plt.title("NLP Boxplot")
plt.show()

plt.boxplot(df["NGP"])
plt.title("NGP Boxplot")
plt.show()

plt.boxplot(df["NGL"])
plt.title("NGL Boxplot")
plt.show()

plt.boxplot(df["NS"])
plt.title("NS Boxplot")
plt.show()

plt.boxplot(df["MHG"])
plt.title("MHG Boxplot")
plt.show()

plt.boxplot(df["GY"])
plt.title("GY Boxplot")
plt.show()

# Correlation between variables
data_corr = df.drop(columns = ['Cultivar'])
sns.heatmap(data_corr.corr(), annot = True, cmap="coolwarm")    
plt.title("Correlation Matrix of df")
plt.show()

### 2. Exploratory Data Analysis (EDA)

# Normalization

scaler = MinMaxScaler()
df_num = df.drop(columns=['Cultivar'])
df_scaled = scaler.fit_transform(df_num)

# Splitting Data into Training and Testing sets
# This will further be used for MHG prediction

x = df_scaled[:, :-2]
y = df_scaled[:, -2] # The target is the second last column, "MHG"
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state= 1)


### 3. Most important factors in determining MHG

corr_matrix = data_corr.corr()
MHG_coef = corr_matrix["MHG"].abs()
max_4 = MHG_coef.nlargest(5)
max_4 = max_4[max_4!=1]
print("Most important factors in determining MHG")
print(max_4)

### 4. Most important factors in determining GY

corr_matrix = data_corr.corr()
MHG_coef = corr_matrix["GY"].abs()
max_4 = MHG_coef.nlargest(5)
max_4 = max_4[max_4!=1]
print("Most important factors in determining GY")
print(max_4)

### 5. Delta in MHG and GY from Season 1 to Season 2   

season1 = df[df["Season"] == 1]
season2 = df[df["Season"] == 2]

season1_mean_MHG = season1["MHG"].mean()
season2_mean_MHG = season2["MHG"].mean()
delta_MHG = season2_mean_MHG - season1_mean_MHG
print(f"Delta in MHG from Season 1 to Season 2 is {delta_MHG}")

season1_mean_GY = season1["GY"].mean()
season2_mean_GY = season2["GY"].mean()
delta_GY = season2_mean_GY - season1_mean_GY
print(f"Delta in GY from Season 1 to Season 2 is {delta_GY}")

### 6. Predict MHG and Clustering - can be found in "models" directory